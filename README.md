## About

This is a simple we application that allows users to build notebooks with a tree structure, where a note can have it's own child notes.

## Installation
1. Clone application, navigate into it's directory
2. run `docker-compose up -d` to start the application
3. run `docker-compose exec php-apache composer install` to install dependencies
4. Navigate to http://localhost:8080 to view the application
## TODO
- Add edit note functionality
- Add delete note funtionality
- Add ability to set note to public
    - Add page to browse public notes
- Improve UI
    - Add error handling
    - Make it more that something is happening when waiting for ajax requests
    - Improve styling
    - Make website usable on mobile devices

## ER Diagram
![ER Diagram](./src/ER_Diagram.dio.png)