<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\NotebookController;
use App\Http\Controllers\NoteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('auth')->group(function () {
    Route::resource('notebooks', NotebookController::class);
    Route::get('/my', [NotebookController::class, 'myNotebooks']);

    Route::resource('notes', NoteController::class);

    Route::get('/logout', function(){
        Auth::logout();
        return Redirect::to('login');
    });
});

Route::get('/', function () {
    return view('home');
});



Auth::routes();
