<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Notebook;

class Note extends Model
{
    use HasFactory;
    
    protected $fillable = array('name', 'content', 'user_id');

    public function notebook()
    {
        return $this->belongsTo(Notebook::class);
    }

    public function parent()
    {
        return $this->belongsTo(Note::class, 'parent_id');
    }
    
    public function directChildren()
    {
        return $this->hasMany(Note::class, 'parent_id');
    }

    // Recursively get all children
    public function children()
    {
        return $this->directChildren()->with('children');
    }
}
