<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Models\Note;
use App\Models\Notebook;

class NoteController extends Controller
{
    
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'content' => 'required',
            'notebook' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
    
        // process the login
        if ($validator->fails()) {
            return redirect('notebooks/create')
                ->withErrors($validator)
                ->withInput($request->all());
        }

        $notebook = Notebook::findOrFail($request->input('notebook'));
        $this->authorize('create', [Note::class, $notebook]);

        // store
        $note = new Note([
            'name' => $request->input('name'),
            'content' => $request->input('content')
        ]);
        $note->notebook()->associate($request->input('notebook'));

        if($request->input('parent')) {
            $note->parent()->associate($request->input('parent'));
        }
        $note->save();

        // redirect
        return $note;
        
    }
}
