<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Models\Notebook;

class NotebookController extends Controller
{
    public function index()
    {      
        return view('notebook.index')
            ->with('notebooks', Notebook::all());
    }

    public function myNotebooks()
    {
        $notebooks = Notebook::where('user_id', Auth::id())->get();
        return view('notebook.index')
            ->with('notebooks', $notebooks);
    }

    public function create()
    {
        return view('notebook.create');
    }

    public function show(Notebook $notebook)
    {
        $this->authorize('view', $notebook);
        
        $notebook = $notebook->load('notes.children');
        return view('notebook.show')->with('notebook', $notebook);
    }

    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'description' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);
    
        // process the login
        if ($validator->fails()) {
            return redirect('notebooks/create')
                ->withErrors($validator)
                ->withInput($request->all());
        }
        // store
        $notebook = new Notebook([
            'name' => $request->input('name'),
            'desc' => $request->input('description')
        ]);
        $notebook->user()->associate(Auth::user());
        $notebook->save();

        // redirect
        return redirect("notebooks/$notebook->id");
        
    }
}
