@extends('layouts.app')

@section('content')
<div>
    <h2 class="text-3xl font-semibold mb-2">{{$notebook->name}}</h2>
    <span class="mb-2">{{$notebook->desc}}</span>

    <notebook-editor :notebook="{{$notebook}}"></notebook-editor>
</div>

@endsection